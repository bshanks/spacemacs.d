;;; SCRIPTY --- Utility functions for shell script conversions
;;
;; Author: Howard Abrams <howard@howardabrams.com>
;; Copyright © 2019, Howard Abrams, all rights reserved.
;; Created: 22 September 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;; A collection of functions helpful in attempting to translate shell
;; scripts into Elisp scripts.

(require 'em-glob)

;;; Code:

(defun scripty--substring-replace (old-str new-str beg end)
  "Return a new string where a subsection of OLD-STR has been
replaced with NEW-STR beginning at position BEG and ending at END."
   (concat (substring old-str 0 beg) new-str (substring old-str end)))

(defun scripty-getvar (var-name)
  "Return value of a variable or environment variable specified
by VAR-NAME."
  (or (getenv var-name) (eval (read var-name))))

(defun scripty--substr-variables (str)
  "Replace shell-like '$VAR' and '${variables}' in STR with the
equivalent environment variables or Elisp variables. For
instance: $HOME/.emacs.d could return /home/howard/.emacs.d --
Keep in mind that this is just a string, it does not do any
validation to see if any files exist."

  ;; This function recursively calls this function with more and more
  ;; embedded variables substituted out, until no more variables are
  ;; found, and then it returns the results.
  ;;
  ;; Begin by checking to see if the string starts with ~ ...
  (if (string-prefix-p "~/" str)
      (scripty--substr-variables
       (concat (getenv "HOME") (substring str 1)))

    ;; Variables can either be simple $BLAH or ${some-larger}...
    (let ((s (or (string-match "${\\([^ }]*\\)}" str)
                 (string-match "$\\([A-z_]*\\)" str)))
          (e (match-end 0)))
      (if (not s)             ; No $ matches?
          str                 ; Then just return the string.
        (scripty--substr-variables  ; Recursively call with first var sub'd
         (scripty--substring-replace str (scripty-getvar (match-string 1 str)) s e))))))

(defun scripty-get-files (path &optional full)
  "Return list of files that match the glob pattern, PATH.
Allowing shell-like variable substitution from the environment,
like $HOME, or from variables defined by `setq'. If FULL is
specified, return absolute pathnames for each file."
  (let ((subbed-path (scripty--substr-variables path)))
    (condition-case nil
        (directory-files (file-name-directory subbed-path)
                         full
                         (eshell-glob-regexp
                          (file-name-nondirectory subbed-path)))
      (error '()))))

(defun scripty-has-org-tag-p (file tag)
  (let ((reg (concat "^#\\+TAGS:.* " tag "\\b")))
    (with-temp-buffer
      (insert-file-contents file)
      (re-search-forward reg nil t 1))))

(defun scripty-get-org-files (path tag &optional full)
  "Return list of `org-mode' files in PATH that match the TAG section.
If FULL is specified, return absolute pathnames for each file."
  (filter (lambda (file) (scripty-has-org-tag-p file tag)) (scripty-get-files path full)))

;; (scripty-get-org-files "~/website/Presentations/*.org" "presentation" t)

(defun scripty-get-path (path &rest extra)
  "Return a file specification based on PATH. We should expand
this function so that glob patterns work when specifying the
parent, but shouldn't worry about matching any particular file.
All EXTRA parameters are appended separated with / characters."
  (let ((file-parts (cons (scripty--substr-variables path) extra)))
    (mapconcat 'identity file-parts "/")))

(defun scripty-shell-if (command &optional expected-results)
  "Runs the COMMAND through the shell, and returns `t' if the
results from standard out matches the value in EXPECTED-RESULTS."
  (-let* ((parts (s-split "[[:blank:]]" command))
          ((ecode results) (apply #'shell-command-with-exit-code parts))
         (trimmed (s-trim results)))
    (if expected-results
        (equal trimmed expected-results)
      (eq ecode 0))))

(defun scripty-ubuntu? ()
  "Returns `t' if called on an Ubuntu system."
  (scripty-shell-if "lsb_release -is" "Ubuntu"))

(defun scripty-mac? ()
  "Returns `t' if called on an Apple Mac system."
  (scripty-shell-if "which osascript"))

(defun scripty-sudo (&rest commands)
  "Run the command strings with superuser privs."
  (let ((default-directory "/sudo:localhost:"))
    (dolist (command commands)
      (shell-command command))))

;; ----------------------------------------------------------------------
;; The following functions are basic "shell" like functions that take
;; a path that refers to files. This allows us to not have to call
;; (scripty-get-files) directly.

(defun scripty-mkdir (path)
  "Create a directory specified by PATH, which can contain
embedded environment variables and Emacs variables, e.g.
'$HOME/Work/foobar'."
  (make-directory (scripty-get-path path) t))

(defun scripty-mkdirs (path)
  "Create one or more directories specified by PATH, which can
contain embedded environment variables and Emacs variables, e.g.
'$HOME/Work/foobar'."
  (mapc (lambda (dir) (make-directory dir t)) (scripty-get-files path)))


(defun scripty-mksymlink (orig link)
  "Create symbolic line to ORIG. If LINK is an existing link, it
is deleted first. LINK could also refer to a directory. Note:
Both parameters are strings that can accept embedded environment
and Lisp variables, e.g. '$HOME/Work/foo.el' and
'${user-emacs-directory}/elisp/bar.el' ."
  (let ((orig-file (scripty-get-path orig))
        (link-file (scripty-get-path link)))

    (if (not (file-symlink-p link-file))
        (make-symbolic-link orig-file link-file t))))

(defun scripty-mksymlinks (files dest)
  "Create an absolute symbolic link for each file specified in
FILES into the directory, DEST (where each link will have the
same name). Both parameters can be specified with glob patterns
and embedded environment and Emacs variables, e.g.
'$HOME/.emacs.d/*.el'."
  (mapc (lambda (file) (scripty-mksymlink file dest)) (scripty-get-files files t)))


(defun scripty-copy-files (from to files)
  "Copy some files FROM a directory TO another directory, where
FILES is a list of names."
  (mapcar (lambda (file)
            (copy-file (scripty-get-path from file)
                       (scripty-get-path to) t))     files))

(provide 'scripty)
;;; scripty ends here
