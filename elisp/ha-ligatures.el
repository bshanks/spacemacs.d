;;; HA LIGATURES
;;
;; Author: Howard Abrams <howard@howardabrams.com>
;; Copyright © 2018, Howard Abrams, all rights reserved.
;; Created: 13 November 2018
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;    Stolen and then modified to get ligatures working with
;;    the Iosevka font as a series of prettify-symbols.
;;
;;    This should work with other fonts as well.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defun replace-unicode ()
  (interactive)
  (while (re-search-forward "#X\\(....\\).*$")
    (let* ((unistr (match-string 1))
           (uninum (string-to-number unistr 16)))
      (replace-match (format "\"%s\") ;; #X%s" (string uninum) unistr)))))

(setq prettify-symbols-unprettify-at-point 'right-edge)

(defun setup-ligatures (&optional version)
  "Attempts to isolate the configuration of prettify as I see
what it takes to get things really working."
  (let* ((linux-ligature-combos '(("lambda" . "λ")
                                  ("-->" . "⥲")
                                  ("->>" . "⥴")
                                  ("->" . "⟶")
                                  (">=" . "≥")
                                  ("<=" . "≤")))
         (mac-ligature-combos '(("lambda". "λ")
                                ;; Double-ended hyphen arrows ----------------
                                ("<->" . "")            ;; "") ;; #Xe100
                                ("<-->" . "")           ;; #Xe101
                                ("<--->" . "")          ;; #Xe102
                                ("<---->" . "")         ;; #Xe103
                                ("<----->" . "")        ;; #Xe104

                                ;; Double-ended equals arrows ----------------
                                ("<=>" . "")            ;; #Xe105
                                ("<==>" . "")           ;; #Xe106
                                ("<===>" . "")          ;; #Xe107
                                ("<====>" . "")         ;; #Xe108
                                ("<=====>" . "")        ;; #Xe109

                                ;; Double-ended asterisk operators ----------------
                                ("<**>" . "")           ;; #Xe10a
                                ("<***>" . "")          ;; #Xe10b
                                ("<****>" . "")         ;; #Xe10c
                                ("<*****>" . "")        ;; #Xe10d

                                ;; HTML comments ----------------
                                ("<!--" . "")         ;; #Xe10e
                                ("<!---" . "")        ;; #Xe10f

                                ;; Three-char ops with discards ----------------
                                ("<$" . "")          ;; #Xe110
                                ("<$>" . "")         ;; #Xe111
                                ("$>" . "")          ;; #Xe112
                                ("<." . "")          ;; #Xe113
                                ("<.>" . "")         ;; #Xe114
                                (".>" . "")          ;; #Xe115
                                ("<*" . "")          ;; #Xe116
                                ("<*>" . "")         ;; #Xe117
                                ("*>" . "")          ;; #Xe118
                                ("<\\" . "")         ;; #Xe119
                                ("<\\>" . "")        ;; #Xe11a
                                ("\\>" . "")         ;; #Xe11b
                                ("</" . "")          ;; #Xe11c
                                ("</>" . "")         ;; #Xe11d
                                ("/>" . "")          ;; #Xe11e
                                ("<\"" . "")         ;; #Xe11f
                                ("<\">" . "")        ;; #Xe120
                                ("\">" . "")         ;; #Xe121
                                ("<'" . "")          ;; #Xe122
                                ("<'>" . "")         ;; #Xe123
                                ("'>" . "")          ;; #Xe124
                                ("<^" . "")          ;; #Xe125
                                ("<^>" . "")         ;; #Xe126
                                ("^>" . "")          ;; #Xe127
                                ("<&" . "")          ;; #Xe128
                                ("<&>" . "")         ;; #Xe129
                                ("&>" . "")          ;; #Xe12a
                                ("<%" . "")          ;; #Xe12b
                                ("<%>" . "")         ;; #Xe12c
                                ("%>" . "")          ;; #Xe12d
                                ("<@" . "")          ;; #Xe12e
                                ("<@>" . "")         ;; #Xe12f
                                ("@>" . "")          ;; #Xe130
                                ("<#" . "")          ;; #Xe131
                                ("<#>" . "")         ;; #Xe132
                                ("#>" . "")          ;; #Xe133
                                ("<+" . "")          ;; #Xe134
                                ("<+>" . "")         ;; #Xe135
                                ("+>" . "")          ;; #Xe136
                                ("<-" . "")          ;; #Xe137
                                ("<->" . "")         ;; #Xe138
                                ("->" . "")          ;; #Xe139
                                ("<!" . "")          ;; #Xe13a
                                ("<!>" . "")         ;; #Xe13b
                                ("!>" . "")          ;; #Xe13c
                                ("<?" . "")          ;; #Xe13d
                                ("<?>" . "")         ;; #Xe13e
                                ("?>" . "")          ;; #Xe13f
                                ("<|" . "")          ;; #Xe140
                                ("<|>" . "")         ;; #Xe141
                                ("|>" . "")          ;; #Xe142
                                ("<:" . "")          ;; #Xe143
                                ("<:>" . "")         ;; #Xe144
                                (":>" . "")          ;; #Xe145

                                ;; Colons ----------------
                                ("::" . "")          ;; #Xe146
                                (":::" . "")         ;; #Xe147
                                ("::::" . "")        ;; #Xe148

                                ;; Arrow-like operators ----------------
                                ("->" . "")             ;; #Xe149
                                ("->-" . "")            ;; #Xe14a
                                ("->--" . "")           ;; #Xe14b
                                ("->>" . "")            ;; #Xe14c
                                ("->>-" . "")           ;; #Xe14d
                                ("->>--" . "")          ;; #Xe14e
                                ("->>>" . "")           ;; #Xe14f
                                ("->>>-" . "")          ;; #Xe150
                                ("->>>--" . "")         ;; #Xe151
                                ("-->" . "")            ;; #Xe152
                                ("-->-" . "")           ;; #Xe153
                                ("-->--" . "")          ;; #Xe154
                                ("-->>" . "")           ;; #Xe155
                                ("-->>-" . "")          ;; #Xe156
                                ("-->>--" . "")         ;; #Xe157
                                ("-->>>" . "")          ;; #Xe158
                                ("-->>>-" . "")         ;; #Xe159
                                ("-->>>--" . "")        ;; #Xe15a
                                (">-" . "")             ;; #Xe15b
                                (">--" . "")            ;; #Xe15c
                                (">>-" . "")            ;; #Xe15d
                                (">>--" . "")           ;; #Xe15e
                                (">>>-" . "")           ;; #Xe15f
                                (">>>--" . "")          ;; #Xe160
                                ("=>" . "")             ;; #Xe161
                                ("=>=" . "")            ;; #Xe162
                                ("=>==" . "")           ;; #Xe163
                                ("=>>" . "")            ;; #Xe164
                                ("=>>=" . "")           ;; #Xe165
                                ("=>>==" . "")          ;; #Xe166
                                ("=>>>" . "")           ;; #Xe167
                                ("=>>>=" . "")          ;; #Xe168
                                ("=>>>==" . "")         ;; #Xe169
                                ("==>" . "")            ;; #Xe16a
                                ("==>=" . "")           ;; #Xe16b
                                ("==>==" . "")          ;; #Xe16c
                                ("==>>" . "")           ;; #Xe16d
                                ("==>>=" . "")          ;; #Xe16e
                                ("==>>==" . "")         ;; #Xe16f
                                ("==>>>" . "")          ;; #Xe170
                                ("==>>>=" . "")         ;; #Xe171
                                ("==>>>==" . "")        ;; #Xe172
                                (">==" . "")            ;; #Xe174
                                (">>=" . "")            ;; #Xe175
                                (">>==" . "")           ;; #Xe176
                                (">>>=" . "")           ;; #Xe177
                                (">>>==" . "")          ;; #Xe178
                                ("<-" . "")             ;; #Xe179
                                ("-<-" . "")            ;; #Xe17a
                                ("--<-" . "")           ;; #Xe17b
                                ("<<-" . "")            ;; #Xe17c
                                ("-<<-" . "")           ;; #Xe17d
                                ("--<<-" . "")          ;; #Xe17e
                                ("<<<-" . "")           ;; #Xe17f
                                ("-<<<-" . "")          ;; #Xe180
                                ("--<<<-" . "")         ;; #Xe181
                                ("<--" . "")            ;; #Xe182
                                ("-<--" . "")           ;; #Xe183
                                ("--<--" . "")          ;; #Xe184
                                ("<<--" . "")           ;; #Xe185
                                ("-<<--" . "")          ;; #Xe186
                                ("--<<--" . "")         ;; #Xe187
                                ("<<<--" . "")          ;; #Xe188
                                ("-<<<--" . "")         ;; #Xe189
                                ("--<<<--" . "")        ;; #Xe18a
                                ("-<" . "")             ;; #Xe18b
                                ("--<" . "")            ;; #Xe18c
                                ("-<<" . "")            ;; #Xe18d
                                ("--<<" . "")           ;; #Xe18e
                                ("-<<<" . "")           ;; #Xe18f
                                ("--<<<" . "")          ;; #Xe190
                                ("=<=" . "")            ;; #Xe192
                                ("==<=" . "")           ;; #Xe193
                                ("<<=" . "")            ;; #Xe194
                                ("=<<=" . "")           ;; #Xe195
                                ("==<<=" . "")          ;; #Xe196
                                ("<<<=" . "")           ;; #Xe197
                                ("=<<<=" . "")          ;; #Xe198
                                ("==<<<=" . "")         ;; #Xe199
                                ("<==" . "")            ;; #Xe19a
                                ("=<==" . "")           ;; #Xe19b
                                ("==<==" . "")          ;; #Xe19c
                                ("<<==" . "")           ;; #Xe19d
                                ("=<<==" . "")          ;; #Xe19e
                                ("==<<==" . "")         ;; #Xe19f
                                ("<<<==" . "")          ;; #Xe1a0
                                ("=<<<==" . "")         ;; #Xe1a1
                                ("==<<<==" . "")        ;; #Xe1a2
                                ("=<" . "")             ;; #Xe1a3
                                ("==<" . "")            ;; #Xe1a4
                                ("=<<" . "")            ;; #Xe1a5
                                ("==<<" . "")           ;; #Xe1a6
                                ("=<<<" . "")           ;; #Xe1a7
                                ("==<<<" . "")          ;; #Xe1a8

                                (">=" . "")             ;; #Xe173
                                ("<=" . "")             ;; #Xe191
                                ;; Monadic operators ----------------
                                (">=>" . "")         ;; #Xe1a9
                                (">->" . "")         ;; #Xe1aa
                                (">-->" . "")        ;; #Xe1ab
                                (">==>" . "")        ;; #Xe1ac
                                ("<=<" . "")         ;; #Xe1ad
                                ("<-<" . "")         ;; #Xe1ae
                                ("<--<" . "")        ;; #Xe1af
                                ("<==<" . "")        ;; #Xe1b0

                                ;; Composition operators ----------------
                                (">>" . "")         ;; #Xe1b1
                                (">>>" . "")        ;; #Xe1b2
                                ("<<" . "")         ;; #Xe1b3
                                ("<<<" . "")        ;; #Xe1b4

                                ;; Lens operators ----------------
                                (":+" . "")        ;; #Xe1b5
                                (":-" . "")        ;; #Xe1b6
                                (":=" . "")        ;; #Xe1b7
                                ("+:" . "")        ;; #Xe1b8
                                ("-:" . "")        ;; #Xe1b9
                                ("=:" . "")        ;; #Xe1ba
                                ("=^" . "")        ;; #Xe1bb
                                ("=+" . "")        ;; #Xe1bc
                                ("=-" . "")        ;; #Xe1bd
                                ("=*" . "")        ;; #Xe1be
                                ("=/" . "")        ;; #Xe1bf
                                ("=%" . "")        ;; #Xe1c0
                                ("^=" . "")        ;; #Xe1c1
                                ("+=" . "")        ;; #Xe1c2
                                ("-=" . "")        ;; #Xe1c3
                                ("*=" . "")        ;; #Xe1c4
                                ("/=" . "")        ;; #Xe1c5
                                ("%=" . "")        ;; #Xe1c6

                                ;; Semigroup/monoid operators ----------------
                                ("<>" . "")         ;; #Xe1c9
                                ("<+" . "")         ;; #Xe1ca
                                ("<+>" . "")        ;; #Xe1cb
                                ("+>" . "")         ;; #Xe1cc
                                ))
         (ligature-combos (if (eq system-type 'darwin)
                              mac-ligature-combos
                            linux-ligature-combos)))
    (dolist (combo ligature-combos)
      (add-to-list 'prettify-symbols-alist combo t
                   (lambda (a b) (equal (car a) (car b)))))

    (defun refresh-pretty ()
      (interactive)
      (prettify-symbols-mode +1))))

(defun ha/get-favorite-font ()
  "Returns my favorite font from what is currently installed."
  (cond
   ((member "Iosevka" (font-family-list)) "Iosevka")
   ;; ((member "Iosevka Nerd Font" (font-family-list)) "Iosevka Nerd Font")
   ((member "Input" (font-family-list)) "Input")
   ;; ((member "Hasklug Nerd Font" (font-family-list)) "Hasklug Nerd Font")
   ((member "Hasklig" (font-family-list)) "Hasklig")
   (t "Source Code Pro")))

(defun ha/get-favorite-font-size (size)
  (format "%s-%d" (ha/get-favorite-font) size))

(defun ha/set-favorite-font-size (size)
  (set-frame-font (ha/get-favorite-font-size size) nil t)

  ;; When using variable-pitch in org, we need to specifically set the
  ;; fixed-pitch as my default fixed-pitched font.
  (custom-theme-set-faces
   'user
   `(fixed-pitch ((t (:family ,(ha/get-favorite-font) :slant normal :weight normal :height 1.0 :width normal)))))

  ;; The following code is Spacemacs specific ... obviously.
  ;; Sets the variable, dotspacemacs-default-font:
  (spacemacs/set-default-font
   `(,(ha/get-favorite-font)
     :size ,size :weight normal :width normal
     :powerline-scale 0.9)))

(defun ha/mac-monitor-fontsize ()
  "Quickly set reset my font size when I connect my laptop to a monitor on a Mac."
  (interactive)
  (ha/set-favorite-font-size 13))

(defun ha/linux-monitor-fontsize ()
  "Quickly set reset my font size when I connect my laptop to a monitor on Linux."
  (interactive)
  (ha/set-favorite-font-size 22))

(defun ha/mac-laptop-fontsize ()
  "Quickly set reset my font size when I disconnect my laptop to a monitor from a Mac."
  (interactive)
  (ha/set-favorite-font-size 32))

(defun ha/linux-laptop-fontsize ()
  "Quickly set reset my font size when I disconnect my laptop to a monitor from Linux."
  (interactive)
  (ha/set-favorite-font-size 32))

(defun ha/ligatures-prettify ()
  (interactive)
  ;; Hooks for modes in which to install the ligatures
  (mapc (lambda (hook)
          (add-hook hook (lambda ()
                           (setup-ligatures)
                           (refresh-pretty))))
        '(text-mode-hook
          prog-mode-hook))

  (global-prettify-symbols-mode +1))

(if (spacemacs/system-is-mac)
    (ha/set-favorite-font-size 15)

  ;; My Linux has a resolution that is too high to be readable:
  (ha/set-favorite-font-size 32))

(ha/ligatures-prettify)
;; (mac-auto-operator-composition-mode t)

(provide 'ha-ligatures)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ha-ligatures.el ends here
