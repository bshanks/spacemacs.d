;;; SCRIPTY-TEST --- Tests for the `scripty' project
;;
;; Author: Howard Abrams <howard@howardabrams.com>
;; Copyright © 2019, Howard Abrams, all rights reserved.
;; Created: 22 September 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(require 'scripty)

(ert-deftest scripty-test-substr-variables ()
  (let ((expected-results (concat (getenv "HOME") "/.emacs.d/elisp/")))
    (should (equal (scripty--substr-variables "$HOME/.emacs.d/elisp/") expected-results))
    (should (equal (scripty--substr-variables "~/.emacs.d/elisp/") expected-results))
    (should (equal (scripty--substr-variables "${user-emacs-directory}elisp/") expected-results))))

(ert-deftest scripty-test-get-files ()
  (should (-contains? (scripty-get-files "./*.el") "scripty-test.el"))
  (should (s-starts-with? "/" (first (scripty-get-files "./*.el" t))))
  (should (null (scripty-get-files "/foo/bar/*"))))

(ert-deftest scripty-test-get-path ()
  (should (equal (scripty-get-path "/home/bar")  "/home/bar"))
  (should (equal (scripty-get-path "$HOME/.emacs.d/elisp/")
                 (concat (getenv "HOME") "/.emacs.d/elisp/")))
  (should (equal (scripty-get-path "/foo/bar" "baz") "/foo/bar/baz"))

  (should
   (let ((blah "blah-blah"))
     (equal (scripty-get-path "/home/${blah}/flubber") "/home/blah-blah/flubber")))

  (should
   (let ((blah "blah-blah") )
     (equal (scripty-get-path "/home/flubber" blah) "/home/flubber/blah-blah"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; scripty-test.el ends here
