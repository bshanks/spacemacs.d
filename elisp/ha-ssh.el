;;; SSH --- Use `vterm' to SSH to hosts from a list
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2016, Howard X. Abrams, all rights reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;    Sure iTerm is nice for connecting and running commands on various remote
;;    systems, however, its worse feature is that it lacks a command line option
;;    that allows you to select and manipulate the displayed text without a mouse.
;;    This is where Emacs can shine.
;;
;;    When you call the `ha/ssh' function, it opens a `vterm' window which,
;;    unlike other terminal emulators in Emacs, merges both Emacs and Terminal
;;    behaviors. Essentially, it just works. It `vterm' isn't installed, it
;;    falls back to `term'.
;;
;;    Preload a list of favorite/special hostnames with multiple calls to:
;;
;;             (ha/ssh-add-favorite-host "Devbox 42" "10.0.1.42")
;;
;;    Then when the `ha/ssh' function is called, a list of hostnames is
;;    available to quickly jump on the system (with the possibility of fuzzy
;;    matching if you have Helm or Ivy installed).
;;
;;    This also has the ability to call OpenStack to gather the hostnames of
;;    dynamic systems (what I call "an Overcloud"), which is appended to the
;;    list of favorite hostnames. The call to OpenStack only needs to be called
;;    once, since the hosts are then cached, see `ha/ssh-overcloud-query-for-hosts'.
;;
;;  Feature Two:
;;
;;    Use the _favorite host_ list to quickly edit a file on a remote system
;;    using Tramp, by calling either `ha/ssh-find-file' and `ha/ssh-find-root'.
;;
;;  Feature Three:
;;
;;    Working with remote shell connections can be completely automated, for
;;    instance:
;;
;;         (let ((win-name "some-host"))
;;            (ha/ssh "some-host.in.some.place" win-name)
;;            (ha/ssh-send "source ~/.bash_profile" win-name)
;;            (ha/ssh-send "clear" win-name))))
;;            ;; ...
;;            (ha/ssh-exit win-name))
;;
;;    Actually the `win-name' in this case is optional, as it will use a good
;;    default.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defvar ha/latest-ssh-window-name nil
  "The window-name of the latest ssh session. Most commands default to the last session.")

(defvar ha/ssh-host-history '() "List of hostnames we've previously connected.")

(defvar ha/ssh-favorite-hostnames '()
  "A list of tuples (associate list) containing a hostname and its IP address.
See `ha/ssh-add-favorite-host' for easily adding to this list.")

(defun ha/ssh-add-favorite-host (hostname ip-address)
  "Add a favorite host to your list for easy pickin's."
  (interactive "sHostname: \nsIP Address: ")
  (add-to-list 'ha/ssh-favorite-hostnames (cons hostname ip-address)))

(defun ha/ssh-choose-host ()
  "Prompts the user for a host, and if it is in the cache, return
its IP address, otherwise, return the input given.
This is used in calls to `interactive' to select a host."
  (let ((hostname
         ;; We call Helm directly if installed, only so that we can get better
         ;; labels in the window, otherwise, the `completing-read' call would be fine.
         (if (fboundp 'helm-comp-read)
             (helm-comp-read "Hostname: " ha/ssh-favorite-hostnames
                             :name "Hosts"
                             :fuzzy t :history ha/ssh-host-history)
           (completing-read "Hostname: " ha/ssh-favorite-hostnames nil 'confirm nil 'ha/ssh-host-history))))
    (alist-get hostname ha/ssh-favorite-hostnames hostname nil 'equal)))

(defun ha/shell (&optional directory)
  "Creates and tidies up a `vterm' terminal shell in side window."
  (interactive "DStarting directory: ")
  (let* ((win-name "Terminal")
         (buf-name (format "*%s*" win-name))
         (default-directory (or directory default-directory)))
    (setq ha/latest-ssh-window-name buf-name)
    (if (not (fboundp 'vterm))
        (make-term win-name "/usr/local/bin/bash")
      (vterm buf-name)
      (ha/ssh-send "source ~/.bash_profile" buf-name)
      (ha/ssh-send "clear" buf-name))))

(defun ha/ssh (hostname &optional window-name)
  "Start a SSH session to a given HOSTNAME (with an optionally specified WINDOW-NAME).
If called interactively, it presents the user with a list
returned by `ha/ssh-choose-host'."
  (interactive (list (ha/ssh-choose-host)))
  (unless window-name
    (setq window-name (format "ssh: %s" hostname)))
  (setq ha/latest-ssh-window-name (format "*%s*" window-name))

  ;; I really like this `vterm' interface, so if I've got it loaded, let's use it:
  (if (not (fboundp 'vterm))
      ;; Should we assume the `ssh' we want is on the PATH that started Emacs?
      (make-term window-name "ssh" nil hostname)
    (vterm ha/latest-ssh-window-name)
    (vterm-send-string (format "ssh %s" hostname))
    (vterm-send-return))

  (pop-to-buffer ha/latest-ssh-window-name))

(defun ha/ssh-send (phrase &optional window-name)
  "Send command PHRASE to the currently running SSH instance.
If you want to refer to another session, specify the correct WINDOW-NAME.
This is really useful for scripts and demonstrations."
  (unless window-name
    (setq window-name ha/latest-ssh-window-name))

  (pop-to-buffer window-name)

  (if (fboundp 'vterm)
      (progn
        (vterm-send-string phrase)
        (vterm-send-return))
    (progn
      (term-send-raw-string phrase)
      (term-send-input))))

(defun ha/ssh-exit (&optional window-name)
  "End the SSH session specified by WINDOW-NAME (or if not, the latest session)."
  (interactive)
  (unless (string-match-p "v?term" (buffer-name))
    (unless window-name
      (setq window-name ha/latest-ssh-window-name))
    (pop-to-buffer window-name))

  (ignore-errors
    (term-send-eof))
  (kill-buffer window-name)
  (delete-window))


(defun ha/ssh-find-file (hostname)
  "Constructs a ssh-based, tramp-focus, file reference, and then calls `find-file'."
  (interactive (list (ha/ssh-choose-host)))
  (let ((tramp-ssh-ref (format "/ssh:%s:" hostname))
        (other-window (when (equal current-prefix-arg '(4)) t)))
    (ha/ssh--find-file tramp-ssh-ref other-window)))

(defun ha/ssh-find-root (hostname)
  "Constructs a ssh-based, tramp-focus, file reference, and then calls `find-file'."
  (interactive (list (ha/ssh-choose-host)))
  (let ((tramp-ssh-ref (format "/ssh:%s|sudo:%s:" hostname hostname))
        (other-window (when (equal current-prefix-arg '(4)) t)))
    (ha/ssh--find-file tramp-ssh-ref other-window)))

(defun ha/ssh--find-file (tramp-ssh-ref &optional other-window)
  "Calls `find-file' after internally completing a file reference based on TRAMP-SSH-REF."
  (let ((tramp-file (read-file-name "Find file: " tramp-ssh-ref)))
    (if other-window
        (find-file-other-window tramp-file)
      (find-file tramp-file))))


;; ----------------------------------------------------------------------
;;   OpenStack Specific Variables and Functions
;; ----------------------------------------------------------------------

(require 'json)

(defvar ha/ssh-overcloud-cache-data nil
  "A vector of associated lists containing the servers in an Overcloud.")

(defun ha/ssh-overcloud-query-for-hosts ()
  "If the overcloud cache hasn't be populated, ask the user if we want to run the command."
  (when (not ha/ssh-overcloud-cache-data)
    (when (y-or-n-p "Cache of Overcloud hosts aren't populated. Retrieve hosts?")
      (ha/ssh-overcloud-cache-populate))))

(advice-add 'ha/ssh-choose-host :before 'ha/ssh-overcloud-query-for-hosts)

(defun ha/ssh-overcloud-cache-populate ()
  (unless (getenv "OS_PASSWORD")
    (setenv "OS_PASSWORD" (read-passwd "Openstack Password: ")))
  (message "Calling the `openstack' command...this will take a while. Grab a coffee, eh?")
  (let ((json-data (shell-command-to-string "openstack server list --no-name-lookup --insecure -f json")))
    (setq ha/ssh-overcloud-cache-data (json-read-from-string json-data))
    (let* ((hostnames (->> ha/ssh-overcloud-cache-data
                           (--map (alist-get 'Name it))))
           (addresses (->> ha/ssh-overcloud-cache-data
                           (--map (alist-get 'Networks it))
                           (--map (replace-regexp-in-string ".*?=" "" it)))))
      (setq ha/ssh-favorite-hostnames (append ha/ssh-favorite-hostnames
                                          (-zip-pair hostnames addresses)))))
  (message "Call to `openstack' complete. Found %d hosts." (length ha/ssh-overcloud-cache-data)))

(defun ha/ssh-overcloud-cache-repopulate ()
  "Repopulate the cache based on redeployment of my overcloud."
  (interactive)
  (setq ha/ssh-overcloud-cache-data nil)
  (ha/ssh-overcloud-cache-populate))

(defun ha/ssh-overcloud (hostname)
  "Log into an overcloud host given by HOSTNAME. Works better if
you have previously run `ssh-copy-id' on the host. Remember, to
make it behave like a real terminal (instead of a window in
Emacs), hit `C-c C-k'."
  (interactive (list (ha/ssh-choose-host)))
  (when (not (string-match-p "\." hostname))
    (setq hostname (format "%s.%s" hostname (getenv "OS_PROJECT_NAME"))))

  (let ((window-label (or (-some->> ha/ssh-favorite-hostnames
                                    (rassoc hostname)
                                    car)
                          hostname)))
    (ha/ssh hostname window-label)
    (sit-for 1)
    (ha/ssh-send "sudo -i")
    (ha/ssh-send (format "export PS1='\\[\\e[34m\\]%s\\[\e[m\\] \\[\\e[33m\\]\\$\\[\\e[m\\] '"
                         window-label))
    (ha/ssh-send "clear")))


(provide 'ha-ssh)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ssh.el ends here
