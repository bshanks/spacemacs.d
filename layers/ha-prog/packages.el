;;; PACKAGES --- Progamming language package configuration
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created:  3 January 2020
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;     Configuration for various programming languages beyond
;;     what others have done in the basic Spacemacs layers.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defconst ha-prog-packages
  '(direnv
    evil-cleverparens
    ;; lsp-docker
    ))

(defun ha-prog/init-direnv ()
  "See https://github.com/wbolster/emacs-direnv for details, for using the
project for managing virtual environments, see https://direnv.net/"
  (use-package direnv
    :ensure t
    :config
    (direnv-mode)))

(defun ha-prog/post-init-evil-cleverparens ()
  "evil-cleverparens (https://github.com/luxbock/evil-cleverparens)
is a keybinding layer (a minor mode for lisp-modes) that gives
evil-lisp-state-like keys to evil's normal state.

The nice feature of this, is very little to learn, as things like
`D' and `dd' work as expected within the s-expressions.

Remember these keybindings:
 • `H' back sexp / `L' forward sexp
 • `(' back to outer sexp / `)' forward outer sexp
 • `M-S-d' cuts the entire s-expression (as `M-Y' and `M-C')
 • `M-a' append to s-exp (Try `M-i' to insert at beginning)
 • `M-o' append a sibling s-exp (try `M-O' for a sibling before)

Anything more complicated, I should kick in `evil-lisp-state'. "
  (setq evil-move-beyond-eol t
        evil-cleverparens-use-s-and-S t
        evil-cleverparens-use-additional-bindings t
        evil-cleverparens-use-additional-movement-keys t)

  ;; The following makes this mode on both Emacs and Clojure:
  (spacemacs/toggle-evil-safe-lisp-structural-editing-on-register-hooks)
  ;; Except it doesn't seem to work, so let's do it the old way, but
  ;; instead of a bunch of calls like:
  ;; (add-hook 'emacs-lisp-mode-hook #'evil-cleverparens-mode)
  (--map (add-hook (-> it
                       symbol-name
                       (concat "-hook")
                       intern)
                   #'evil-cleverparens-mode)
         evil-lisp-safe-structural-editing-modes)

  ;; Since the :diminish doesn't work with spaceline, we:
  (spacemacs|hide-lighter evil-cleverparens-mode))

(defun ha-prog/init-lsp-docker (a bbob c)
  "Trying out https://github.com/emacs-lsp/lsp-docker"
  (use-package lsp-docker
    :init
    (setq lsp-docker-client-packages '(lsp-clients lsp-pyls lsp-bash)
          lsp-docker-client-configs
          '((:server-id bash-ls :docker-server-id bashls-docker :server-command "bash-language-server start")
            (:server-id pyls :docker-server-id pyls-docker :server-command "pyls")))
    :config
    (lsp-docker-init-clients
     :path-mappings `((,(getenv "HOME") . ,(getenv "HOME")))
     :docker-image-id "wpc-monitoring"
     :docker-container-name "wpc-monitoring"
     :client-packages lsp-docker-client-packages
     :client-configs lsp-docker-client-configs)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; packages.el ends here
