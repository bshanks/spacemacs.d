;;; PACKAGES --- Package configuration for coding in Emacs Lisp
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created: 12 May 2020
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defconst ha-emacs-lisp-packages
  '(emacs-lisp
    overseer
    paren
    paren-face
    suggest))

(defun ha-emacs-lisp/post-init-emacs-lisp ()
  "Configure the emacs lisp editing environment."
  (add-hook 'emacs-lisp-mode-hook
            (lambda ()
              (make-local-variable 'evil-args-delimiters)
              (setq evil-args-delimiters '(" ")))))


(defun ha-emacs-lisp/post-init-overseer ()
  "An ERT runner system for a project. Newly added/configured on
the `develop' branch of Spacemacs. For details, see
https://github.com/tonini/overseer.el"
  (use-package overseer
    :config
    (add-to-list 'overseer--project-root-indicators ".git")))

(defun ha-emacs-lisp/init-paren ()
  "The reverse mode of the default parenthesis matching doesn’t match well,
  and this package makes it bold and more obvious. "
  (use-package paren
    :init
    (set-face-background 'show-paren-match (face-background 'default))
    (set-face-foreground 'show-paren-match "#afa")
    (set-face-attribute  'show-paren-match nil :weight 'black)
    (set-face-background 'show-paren-mismatch (face-background 'default))
    (set-face-foreground 'show-paren-mismatch "#c66")
    (set-face-attribute  'show-paren-mismatch nil :weight 'black)))

(defun ha-emacs-lisp/init-paren-face ()
  "See https://github.com/tarsius/paren-face for details on dimming parens."
  (use-package paren-face
    :ensure t
    :config
    (set-face-foreground 'parenthesis "#666")
    (global-paren-face-mode)))

(defun ha-emacs-lisp/init-suggest ()
  "This separate application helps me find the Lisp function
based on behavior (input and output). Call its main function:
suggest"
  (use-package suggest
    :ensure t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; packages.el ends here
