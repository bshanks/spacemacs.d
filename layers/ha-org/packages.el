;;; packages.el --- my-org layer packages file for Spacemacs.
;;
;; Author: Howard Abrams <howard@google.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3
;;
;;; Commentary:
;;
;; See the Spacemacs documentation and FAQs for instructions on
;; how to implement a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this
;; layer should be added to `ha-org-packages'. Then, for each
;; package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer,
;;   define a function `ha-org/init-PACKAGE' to load and
;;   initialize the package.
;;
;; - Otherwise, PACKAGE is already referenced by another
;;   Spacemacs layer, so define the functions
;;   `ha-org/pre-init-PACKAGE' and/or `ha-org/post-init-PACKAGE'
;;   to customize the package as it is loaded.
;;
;;; Code:

(defconst ha-org-packages
  '(org
    org-journal
    org-tree-slide
    org-bullets
    org-beautify-theme
    org-mime
    org-ql
    org-variable-pitch

    adaptive-wrap
    evil-surround
    graphviz-dot-mode
    plantuml-mode
    writegood-mode

    (boxes :location local)))

(defun ha-org/post-init-org ()
  (setq org-return-follows-link t
        ;; Speed Commands: If point is at the beginning of a headline or code
        ;; block in org-mode, single keys do fun things. See
        ;; org-speed-command-help for details (or hit the ? key at a headline).
        org-use-speed-commands t
        org-pretty-entities t
        org-hide-emphasis-markers t

        org-ellipsis "⤵"     ; …, ➡, ⚡, ▼, ↴, , ∞, ⬎, ⤷, ⤵
        org-agenda-breadcrumbs-separator " ❱ "
        org-directory "~/personal"
        org-completion-use-ido t
        org-outline-path-complete-in-steps nil
        org-src-fontify-natively t ;; Pretty code blocks
        org-src-tab-acts-natively t
        org-confirm-babel-evaluate nil
        org-agenda-span 'day ; Default is 'week
        org-todo-keywords '((sequence "TODO(t)" "DOING(g)" "|" "DONE(d)" )
                            (sequence "BLOCKED(b)" "|" "CANCELED(c)"))
        org-plantuml-jar-path (concat (getenv "HOME") "/bin/plantuml.jar"))

  (add-to-list 'auto-mode-alist '("\\.txt\\'" . org-mode))
  (add-to-list 'auto-mode-alist '(".*/[0-9]*$" . org-mode)) ;; Journal entries
  (add-hook 'org-mode-hook 'yas-minor-mode-on)

  ;; With the new feature where all attributes, including language is shown in
  ;; the mini-buffer, we can shrink the borders to make them less noticeable.
  (face-spec-set 'org-block-begin-line
                 '((t (:foreground "#99a" :background "#334" :height 0.8))))
  (face-spec-set 'org-block-end-line
                 '((t (:foreground "#99a" :background "#334" :height 0.6))))
  (face-spec-set 'org-level-3 '((t (:family "Sans Serif"))))
  (face-spec-set 'org-level-4 '((t (:family "Sans Serif"))))
  (face-spec-set 'org-level-5 '((t (:family "Sans Serif"))))

  (font-lock-add-keywords 'org-mode
                          '(("^ +\\([-*]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  (font-lock-add-keywords             ; A bit silly but my headers are now
   'org-mode `(("^\\*+ \\(TODO\\) "   ; shorter, and that is nice canceled
                (1 (progn (compose-region (match-beginning 1) (match-end 1) "⚑")
                          nil)))
               ("^\\*+ \\(DOING\\) "
                (1 (progn (compose-region (match-beginning 1) (match-end 1) "⚐")
                          nil)))
               ("^\\*+ \\(CANCELED\\) "
                (1 (progn (compose-region (match-beginning 1) (match-end 1) "✘")
                          nil)))
               ("^\\*+ \\(DONE\\) "
                (1 (progn (compose-region (match-beginning 1) (match-end 1) "✔")
                          nil)))
               ;; Here is my approach for quickly making the
               ;; initial asterisks for listing items and
               ;; whatnot, appear as Unicode bullets (without
               ;; actually affecting the text file or the
               ;; behavior).
               ("^ +\\([-*]\\) "
                (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  (spacemacs|define-transient-state org-movement
    :title "Movement in org-mode files"
    :doc "\n[_p_] previous heading [_n_] next heading [_u_] up to parent [_q_] quit"
    :bindings
    ("p" org-previous-visible-heading)
    ("n" org-next-visible-heading)
    ("u" outline-up-heading)
    ("j" org-next-item)
    ("k" org-previous-item)
    ("h" org-beginning-of-item-list)
    ("l" org-end-of-item-list)
    ("q" nil :exit t))

  (ha/set-leader-keys-for-major-mode 'org-mode "o g"
                                     '("org-movement" . spacemacs/org-movement-transient-state/body))

  ;; I like the idea of more movement within org-files, so I may want to add
  ;; the following:

  (spacemacs/set-leader-keys-for-major-mode 'org-mode
    "k"  'org-previous-visible-heading
    "j"  'org-next-visible-heading
    "[" 'org-beginning-of-item-list
    "]" 'org-end-of-item-list)

  ;; :bind (:map org-mode-map
  ;;             ("M-C-n" . org-end-of-item-list)
  ;;             ("M-C-p" . org-beginning-of-item-list)
  ;;             ("M-C-u" . outline-up-heading)
  ;;             ("M-C-w" . org-table-copy-region)
  ;;             ("M-C-y" . org-table-paste-rectangle))

  (use-package ox-md)
  (use-package ox-confluence
    :load-path "~/.spacemacs.d/elisp"
    :config
    (ha/set-leader-keys-for-major-mode 'org-mode
                                       "e c" '("export-to-confluence" . org-confluence-export-as-confluence)))
  (use-package ox-html
    :init
    (setq org-html-postamble nil
          org-export-with-section-numbers nil
          org-export-with-toc nil
          org-html-head-extra "
          <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
          <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:400,700' rel='stylesheet' type='text/css'>
          <style type='text/css'>
             body {
                font-family: 'Source Sans Pro', sans-serif;
             }
             pre, code {
                font-family: 'Source Code Pro', monospace;
             }
          </style>"))

  ;; Trying an experiment to see if I like inserting two spaces
  ;; at the end of a sentence, or should I do:
  ;; (setq sentence-end-double-space nil)
  (defun ha/insert-two-spaces (N)
    "Inserts two spaces at the end of sentences."
    (interactive "p")
    (when (looking-back "[!?.] " 2)
      (insert " ")))

  (advice-add 'org-self-insert-command :after #'ha/insert-two-spaces)

  (use-package ox-rss)

  (use-package ox-publish
    :config
    (defun org-mode-blog-preamble (options)
      "The function that creates the preamble top section for the blog.
    OPTIONS contains the property list from the org-mode export."
      (let ((base-directory (plist-get options :base-directory)))
        (org-babel-with-temp-filebuffer (expand-file-name "top-bar.html" base-directory) (buffer-string))))

    (defun org-mode-blog-postamble (options)
      "The function that creates the postamble, or bottom section for the blog.
  OPTIONS contains the property list from the org-mode export."
      (let ((base-directory (plist-get options :base-directory)))
        (org-babel-with-temp-filebuffer (expand-file-name "bottom.html" base-directory) (buffer-string))))

    (defun org-mode-blog-prepare (&optional options)
      "`index.org' should always be exported so touch the file before publishing."
      (let* ((base-directory (plist-get options :base-directory))
             (buffer (find-file-noselect (expand-file-name "index.org" base-directory) t)))
        (with-current-buffer buffer
          (set-buffer-modified-p t)
          (save-buffer 0))
        (kill-buffer buffer)))

    (setq org-mode-websrc-directory (concat (getenv "HOME") "/website"))
    (setq org-mode-publishing-directory (concat (getenv "HOME") "/Public/"))

    (setq org-publish-project-alist
          `(("all"
             :components ("blog-content" "blog-static" "org-notes" "blog-rss"))

            ("blog-content"
             :base-directory       ,org-mode-websrc-directory
             :base-extension       "org"
             :publishing-directory ,org-mode-publishing-directory
             :recursive            t
             :publishing-function  org-html-publish-to-html
             :preparation-function org-mode-blog-prepare
             :export-with-tags     nil
             :headline-levels      4
             :auto-preamble        t
             :auto-postamble       nil
             :auto-sitemap         t
             :sitemap-title        "Howardisms"
             :section-numbers      nil
             :table-of-contents    nil
             :with-toc             nil
             :with-author          nil
             :with-creator         nil
             :with-tags            nil
             :with-smart-quotes    t

             :html-doctype         "html5"
             :html-html5-fancy     t
             :html-preamble        org-mode-blog-preamble
             :html-postamble       org-mode-blog-postamble
             ;; :html-postamble "<hr><div id='comments'></div>"
             :html-head  "<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
              <link href='http://fonts.googleapis.com/css?family=Source+Serif+Pro:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
              <link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:400,700' rel='stylesheet' type='text/css'>
              <link rel=\"stylesheet\" href=\"/css/styles.css\" type=\"text/css\"/>\n"
             :html-head-extra "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
              <script src=\"/js/magic.js\"></script>
              <link rel=\"icon\" href=\"/img/dragon.svg\">
              <link rel=\"shortcut icon\" href=\"/img/dragon-head.png\">
              <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />"
             :html-head-include-default-style nil
             )

            ("blog-static"
             :base-directory       ,org-mode-websrc-directory
             :base-extension       "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|svg"
             :publishing-directory ,org-mode-publishing-directory
             :recursive            t
             :publishing-function  org-publish-attachment
             )

            ("blog-rss"
             :base-directory        ,org-mode-websrc-directory
             :base-extension        "org"
             :rss-image-url         "http://howardism.org/img/dragon-head.png"
             :publishing-directory  ,org-mode-publishing-directory
             :publishing-function   (org-rss-publish-to-rss)
             :html-link-home        "http://www.howardism.org/"
             :html-link-use-abs-url t
             :with-toc              nil
             :exclude               ".*"
             :include               ("index.org"))

            ("org-notes"
             :base-directory        "~/technical/"
             :base-extension       "org"
             :publishing-directory ,(concat org-mode-publishing-directory "/notes/")
             :recursive            t
             :publishing-function  org-html-publish-to-html
             :headline-levels      4  ; Just the default for this project.
             :auto-preamble        t
             :auto-sitemap         t  ; Generate sitemap.org automagically...
             :makeindex            t
             :section-numbers      nil
             :table-of-contents    nil
             :with-author          nil
             :with-creator         nil
             :with-tags            nil
             :style                "<link rel=\"stylesheet\" href=\"../css/styles.css\" type=\"text/css\"/> <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\" type=\"text/javascript\"></script> <link href=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/smoothness/jquery-ui.css\" type=\"text/css\" rel=\"stylesheet\" />    <script src=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js\" type=\"text/javascript\"></script> <script =\"text/javascript\" src=\"js/magic.js\"></script>"
             )

            ("org-notes-static"
             :base-directory       "~/technical/"
             :base-extension       "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
             :publishing-directory ,(concat org-mode-publishing-directory "/other/")
             :recursive            t
             :publishing-function  org-publish-attachment
             ))))

  ;; I've notice that while I really like taking notes in a meeting, I
  ;; don't always like the multiple windows I have opened, so I created
  ;; this function that I can easily call to eliminate distractions
  ;; during a meeting.

  (defun meeting-notes ()
    "Call this after creating an org-mode heading for where the notes for the meeting
     should be. After calling this function, call 'meeting-done' to reset the environment."
      (interactive)
      (outline-mark-subtree)                              ;; Select org-mode section
      (narrow-to-region (region-beginning) (region-end))  ;; Only show that region
      (deactivate-mark)
      (delete-other-windows)                              ;; Get rid of other windows
      (text-scale-set 2)                                  ;; Text is now readable by others
      (fringe-mode 0)
      (message "When finished taking your notes, run meeting-done."))

    ;; Of course, I need an 'undo' feature when the meeting is over...

    (defun meeting-done ()
      "Attempt to 'undo' the effects of taking meeting notes."
      (interactive)
      (widen)                                       ;; Opposite of narrow-to-region
      (text-scale-set 0)                            ;; Reset the font size increase
      (fringe-mode 1)
      (winner-undo))                                ;; Put the windows back in place

    (setq org-default-notes-file "~/personal/general-notes.txt")

    (defun ha/first-header ()
      (goto-char (point-min))
      (search-forward-regexp "^\* ")
      (beginning-of-line 1)
      (point))

    (defvar org-capture-templates (list))
    (setq org-capture-default-template "c")

    (add-to-list 'org-capture-templates
                 '("c" "Currently clocked in task"))
    (add-to-list 'org-capture-templates
                 `("cc" "Item to Current Clocked Task" item
                   (clock)
                   "%i%?" :empty-lines 1))
    (add-to-list 'org-capture-templates
                 `("cr" "Contents to Current Clocked Task" plain
                   (clock)
                   "%i" :immediate-finish t :empty-lines 1))
    (add-to-list 'org-capture-templates
                 `("ck" "Kill-ring to Current Clocked Task" plain
                   (clock)
                   "%c" :immediate-finish t :empty-lines 1))
    (add-to-list 'org-capture-templates
                 `("cf" "Code Reference with Comments to Current Task"
                   plain (clock)
                   "%(ha/org-capture-code-snippet \"%F\")\n\n   %?"
                   :empty-lines 1))
    (add-to-list 'org-capture-templates
                 `("cl" "Link to Code Reference to Current Task"
                   plain (clock)
                   "%(ha/org-capture-code-snippet \"%F\")"
                   :empty-lines 1 :immediate-finish t))

    (add-to-list 'org-capture-templates
                 '("n" "Thought or Note"  entry
                   (file org-default-notes-file)
                   "* %?\n\n  %i\n\n  See: %a" :empty-lines 1))
    (add-to-list 'org-capture-templates
                 '("j" "Journal Note"     entry
                   (file (get-journal-file-today))
                   "* %?\n\n  %i\n\n  From: %a" :empty-lines 1))
    (add-to-list 'org-capture-templates
                 '("w" "Website Announcement" entry
                   (file+function "~/website/index.org" ha/first-header)
                   (file "~/.spacemacs.d/templates/website-announcement.org")
                   :empty-lines 1))

  ;; The trick to literate programming is in the
  ;; [[http://orgmode.org/worg/org-contrib/babel/intro.html][Babel project]],
  ;; which allows org-mode to not only interpret source code blocks, but
  ;; evaluate them and tangle them out to a file.
  (add-to-list 'org-src-lang-modes '("dot" . "graphviz-dot"))

  (org-babel-do-load-languages 'org-babel-load-languages
                               '((shell      . t)
                                 (js         . t)
                                 (emacs-lisp . t)
                                 (clojure    . t)
                                 (python     . t)
                                 (sql        . t)
                                 (ruby       . t)
                                 (dot        . t)
                                 (css        . t)
                                 (plantuml   . t))))

(defun ha-org/init-adaptive-wrap ()
  "Integrate the adaptive-wrap package with org-mode."
  (use-package adaptive-wrap
    :ensure t
    :config (add-hook 'org-mode-hook 'ha/org-for-word-processing)))

(defun ha-org/post-init-org-journal ()
  "Additional configuration for org-journal."
  (use-package org-journal
    :ensure t))

(defun ha-org/post-init-org-mime ()
  (use-package org-mime
    :ensure t))


(defun ha-org/init-org-ql ()
  "See https://github.com/alphapapa/org-ql"
  (use-package org-ql
    :ensure t))

(defun ha-org/init-org-super-agenda ()
  "See https://github.com/alphapapa/org-super-agenda"
  (use-package org-super-agenda
    :ensure t))

(defun ha-org/init-org-tree-slide()
  (use-package org-tree-slide
    :ensure t
    :init
    (setq org-tree-slide-skip-outline-level 4)
    (org-tree-slide-simple-profile)))

(defun ha-org/post-init-org-bullets ()
  (add-hook 'org-mode-hook 'org-bullets-mode))

(defun ha-org/init-org-beautify-theme ()
  (use-package org-beautify-theme
    :ensure t
    :init
    (load-theme 'org-beautify)
    :config
    (custom-theme-set-faces
     'user
     '(org-level-3 ((t :weight semibold))))))

(defun ha-org/init-org-variable-pitch ()
  "See https://dev.gkayaalp.com/elisp/index.html#ovp"
  (use-package org-variable-pitch
    :ensure t
    :after org-mode
    :init
    (add-hook 'org-mode-hook 'org-variable-pitch-minor-mode)
    :config
    (custom-theme-set-faces
     'user
     '(variable-pitch ((t (:family "Source Sans Pro" :height 1.2 :weight light))))
     '(org-block ((t (:inherit fixed-pitch))))
     '(org-block-begin-line ((t (:inherit fixed-pitch :foreground "#99a" :background "#334" :height 0.8))))
     '(org-block-end-line ((t (:inherit fixed-pitch :foreground "#99a" :background "#334" :height 0.8))))
     '(org-link ((t (:foreground "#81a2be" :underline t))))
     )))

(defun ha-org/init-ox-reveal ()
  "Generate presentations from my org-mode files using
https://github.com/yjwen/org-reveal. Just download and make the
results available to the HTML output."
  (use-package ox-reveal
    :ensure t
    :defer t
    :init
    (setq org-reveal-postamble "Howard Abrams"
          org-reveal-root (concat "file://" (getenv "HOME") "/Public/js/reveal.js"))))

(defun ha-org/post-init-evil-surround ()

  "Add more pairs for org-specific text phrases to surround text."
  (use-package evil-surround
    :config
    ;; Put quotes around the next three words: 3 g s w "
    (define-key evil-normal-state-map (kbd "gs") 'evil-surround-edit)

    (add-hook 'org-mode-hook
              (lambda ()
                (push '(?b . ("*" . "*")) evil-surround-pairs-alist)
                (push '(?i . ("/" . "/")) evil-surround-pairs-alist)
                (push '(?u . ("_" . "_")) evil-surround-pairs-alist)
                (push '(?c . ("~" . "~")) evil-surround-pairs-alist)
                (push '(?v . ("=" . "=")) evil-surround-pairs-alist)
                (push '(?s . ("#+BEGIN_SRC " . "#+END_SRC")) evil-surround-pairs-alist)
                (push '(?e . ("#+BEGIN_EXAMPLE" . "#+END_EXAMPLE")) evil-surround-pairs-alist)
                (push '(?q . ("#+BEGIN_QUOTE" . "#+END_QUOTE")) evil-surround-pairs-alist)))))

(defun ha-org/init-ox-md ()
  (use-package ox-md))

(defun ha-org/init-graphviz-dot-mode ()
  (use-package graphviz-dot-mode
    :ensure t
    :mode "\\.dot\\'"
    :init
    (setq tab-width 4
          graphviz-dot-indent-width 2
          graphviz-dot-auto-indent-on-newline t
          graphviz-dot-auto-indent-on-braces t
          graphviz-dot-auto-indent-on-semi t)))

(defun ha-org/init-plantuml-mode ()
  (use-package plantuml-mode
    :ensure t))

(defun ha-org/init-writegood-mode ()
  "Highlights passive and weasel words as typed.
See https://github.com/bnbeckwith/writegood-mode.
If only it also checked for dangled prepositions."
  (use-package writegood-mode
    :hook ((org-mode . writegood-mode))))

(defun ha-org/init-boxes ()
  (use-package boxes
    :after (org)
    :load-path "~/.spacemacs.d/elisp"
    :config
    (spacemacs/set-leader-keys-for-major-mode 'org-mode "r" 'hydra-org-refiler/body)

    (spacemacs|define-custom-layout "projects"
      :binding "x"
      :body (progn
              (persp-switch "projects")
              (projectile-switch-project-by-name "~/projects")
              (org-boxes-workflow)))))

;;; packages.el ends here
