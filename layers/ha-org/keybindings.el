;;; KEYBINDINGS --- Special org-mode keybindings from this layer
;;
;; Author: Howard Abrams <howard.abrams@gmail.com>
;; Copyright © 2018, Howard Abrams, all rights reserved.
;; Created: 19 July 2018
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;; Bring the keybindings to functions in the `funcs.el' file to a single
;; location to make it more obvious what is available.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(spacemacs/set-leader-keys-for-major-mode 'org-mode "i N" 'org-insert-name)

(spacemacs/declare-prefix-for-mode 'org-mode "o" "personal")

(spacemacs/declare-prefix "o a" "agenda")
(ha/set-leader-keys
 "o a a" 'org-ql-view
 "o a t" '("todays-agenda" . ha/todays-agenda))

(spacemacs/declare-prefix "o o" "org")
(ha/set-leader-keys
 "o o C" '("code-to-clocked" . ha/code-to-clock)
 "o o c" '("comment-code-to-clocked" . ha/code-comment-to-clock))

(spacemacs/declare-prefix "o f" "files")
(spacemacs/set-leader-keys
  "o f j" 'journal-file-today
  "o f y" 'journal-file-yesterday
  "o f Y" 'journal-last-year)

(spacemacs/declare-prefix-for-mode 'org-mode "o y" "clipboard")
(ha/set-leader-keys-for-major-mode 'org-mode
  "o y y" '("org-yank" . ha/org-yank-clipboard)
  "o y h" '("to-html" . ha/org-html-to-clipboard)
  "o y H" '("to-html-header" . ha/org-html-with-header-to-clipboard)
  "o y s" '("to-slack" . ha/org-to-slack-clipboard)
  "o y m" '("to-markdown" . ha/org-to-md-clipboard))

(global-set-key (kbd "s-C-M X") 'ha/org-yank-clipboard)

;; While I don't like to mess with standardly-chosen keybindings:
(spacemacs/set-leader-keys-for-major-mode 'org-mode "u" 'spacemacs/org-movement-transient-state/outline-up-heading)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; keybindings.el ends here
