;;; OTHER --- Summary
;;
;; Author: Howard Abrams <howard@howardabrams.com>
;; Copyright © 2020, Howard Abrams, all rights reserved.
;; Created: 29 March 2020
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Change log:
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defun ha/sprint-agenda ()
  "Display an agenda based on the work for my current sprint."
  (interactive)
  (org-ql-view "Overview: Sprint"))

(ha/set-leader-keys "o a s" '("sprint-agenda" . ha/sprint-agenda))

(add-to-list 'org-ql-views
             (cons "Overview: Sprint"
                   (list :buffers-files #'org-agenda-files
                         :query `(or (and (done)
                                          (ts :on today))
                                     ;; Don't show habits if they have been completed today:
                                     (and (habit)
                                          (not (done))
                                          (scheduled :to today))
                                     (and ,ha/org-ql-typical-work-tasks
                                          ;; Only show tasks that are scheduled during sprint:
                                          ts :from ,(sprint-day-start) :to ,(sprint-day-end)))
                         :sort '(date priority)
                         :super-groups 'org-super-agenda-groups
                         :title "Sprint Work")))

(add-to-list 'org-ql-views
             (cons "Calendar: Sprint"
                   (list :buffers-files #'org-agenda-files
                         :query '(scheduled :from tomorrow :to tomorrow)
                         :super-groups '((:auto-planning)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; other.el ends here
