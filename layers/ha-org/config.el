;;; CONFIG --- Settings of my org-mode files
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created: 23 February 2020
;;
;;; Code:

(with-eval-after-load "org"
  (add-to-list 'org-modules 'org-protocol)
  (setq org-super-agenda-date-format "%A (%e)")

  ;; Favorite Org Queries
  (setq ha/org-ql-typical-work-tasks
        '(and (tags "work")

              ;; I will always be Supporting the onboarding projects,
              ;; but don't show them (but show descendants):
              (not (and (tags "onboarding")
                        (heading "^Support ")))

              ;; Show a blocking subtask instead of the parent:
              (or (not (children))
                  (not (descendants (todo "TODO" "DOING"))))

              (not (habit))
              (not (done))))

  (setq org-super-agenda-groups
        '((:name "Accomplishments"
                 :todo ("DONE" "CANCELED")
                 :order 4)
          (:name "End of Day"
                 :habit t
                 :order 2)
          (:name "Uncompleted Work"
                 :todo "DOING"
                 :scheduled past
                 :order 0)
          (:name "Today's Tasks"
                 :date today
                 :order 1)
          (:name "Today's Tasks"
                 :scheduled today
                 :order 1)
          (:name "Future Work"
                 :todo "TODO"
                 :scheduled future
                 :order 3)))

  (setq ha/org-super-agenda-today
        '((:name "Finished"
                 :todo ("DONE" "CANCELED")
                 :order 4)
          (:name "End of Day"
                 :habit t
                 :order 2)
          (:name "Today's Tasks"
                 :todo "DOING"
                 :scheduled past
                 :date today
                 :order 0)))

  (setq org-ql-views
        (list (cons "Overview: Today"
                    (list :buffers-files #'org-agenda-files
                          :query `(or (closed :on today)
                                      (and (habit)
                                           (not (done))
                                           (scheduled :to today))
                                      (and ,ha/org-ql-typical-work-tasks
                                           (or (deadline auto)
                                               (todo "DOING")
                                               (scheduled :to today)
                                               (ts-active :on today))))
                          :sort '(priority date)
                          :super-groups 'ha/org-super-agenda-today
                          :title "Today in Me"))

              (cons "Overview: Tomorrow"
                    (list :buffers-files #'org-agenda-files
                          :query '(and (not (done))
                                       (tags "work")
                                       (scheduled :from tomorrow :to tomorrow))
                          :sort '(priority date)
                          :super-groups 'ha/org-super-agenda-today
                          :title "Overview: Tomorrow's tasks"))

              (cons "Calendar: Today"
                    (list :buffers-files #'org-agenda-files
                          :query '(ts-active :on today)
                          :title "Today"
                          :super-groups 'ha/org-super-agenda-today
                          :sort '(priority))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; config.el ends here
