;;; KEYBINDINGS --- Miscellaneous keybindings
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2018, Howard X. Abrams, all rights reserved.
;; Created: 17 December 2018
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(when (spacemacs/system-is-mac)
  (setq mac-option-modifier 'meta))

(global-set-key (kbd "s-=") 'spacemacs/scale-up-font)
(global-set-key (kbd "s--") 'spacemacs/scale-down-font)
(global-set-key (kbd "s-0") 'spacemacs/reset-font-size)

(global-set-key (kbd "s-v") 'yank)
(global-set-key (kbd "s-c") 'evil-yank)
(global-set-key (kbd "s-a") 'mark-whole-buffer)
(global-set-key (kbd "s-x") 'kill-region)
(global-set-key (kbd "s-z") 'undo-tree-undo)

(ha/set-leader-keys
 "o S" '("laptop-sleep" . ha/laptop-sleep)
 "o W" '("laptop-wake" . ha/laptop-wakeup)
 "o n" '("next-git-change" . spacemacs/vcs-next-hunk)
 "o p" '("previous-git-change" . spacemacs/vcs-previous-hunk))

;; Handy key definition
(define-key global-map "\M-Q" 'unfill-paragraph)

(spacemacs/set-leader-keys
  "x q" 'fill-paragraph
  "x Q" 'unfill-paragraph
  "x d l" 'just-one-line
  "x d L" 'delete-blank-lines)

;;  Let's make moving from function to function similar to moving from header to
;;  header in org-mode:
(dolist (mode '(prog-mode emacs-lisp-mode ruby-mode))
  (spacemacs/set-leader-keys-for-major-mode mode
    "j" 'beginning-of-defun
    "k" 'end-of-defun))

(define-key prog-mode-map (kbd "C-<return>") 'newline-for-code)

;; Use the `bol-with-prefix' function to enhance some functions:
(global-set-key (kbd "C-k") (bol-with-prefix kill-line))
(global-set-key [remap paredit-kill] (bol-with-prefix paredit-kill))
(global-set-key [remap sp-kill-hybrid-sexp] (bol-with-prefix sp-kill-hybrid-sexp))
(global-set-key [remap org-kill-line] (bol-with-prefix org-kill-line))
(global-set-key [remap kill-line] (bol-with-prefix kill-line))


(add-hook 'org-mode-hook (lambda()
                           (define-key org-mode-map [remap org-transpose-words] #'ha/transpose-words)))
(global-set-key [remap transpose-words] #'ha/transpose-words)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; keybindings.el ends here
