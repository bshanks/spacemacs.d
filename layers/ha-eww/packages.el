;;; packages.el --- ha-eww layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author: Howard X. Abrams <howard.abrams@workday.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `ha-eww-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `ha-eww/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `ha-eww/pre-init-PACKAGE' and/or
;;   `ha-eww/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst ha-eww-packages
  '(eww)
  "The list of Lisp packages required by the ha-eww layer.

Each entry is either:

1. A symbol, which is interpreted as a package to be installed, or

2. A list of the form (PACKAGE KEYS...), where PACKAGE is the
    name of the package to be installed or loaded, and KEYS are
    any number of keyword-value-pairs.

    The following keys are accepted:

    - :excluded (t or nil): Prevent the package from being loaded
      if value is non-nil

    - :location: Specify a custom installation location.
      The following values are legal:

      - The symbol `elpa' (default) means PACKAGE will be
        installed using the Emacs package manager.

      - The symbol `local' directs Spacemacs to load the file at
        `./local/PACKAGE/PACKAGE.el'

      - A list beginning with the symbol `recipe' is a melpa
        recipe.  See: https://github.com/milkypostman/melpa#recipe-format")

(defun ha-eww/init-eww ()
  "Emacs Web Wowzer configuration location."
  (use-package eww
    :init
    (setq browse-url-browser-function 'eww-browse-url
          shr-use-colors nil
          shr-bullet "• "
          shr-folding-mode t
          eww-search-prefix "https://duckduckgo.com/html?q="
          url-privacy-level '(email agent cookies lastloc))

    :config
    (add-hook 'eww-after-render-hook 'ha/eww-rerenderer)

    (spacemacs/set-leader-keys
      "o b" 'eww
      "o B" 'ha/eww-wiki)

    ;; Many of these are repeats of the existing non-comma keysquences, or those
    ;; that might have been overshadowed by the normal state:
    (ha/set-leader-keys-for-major-mode 'eww-mode
      ","  '("scroll"         . scroll-up-command)
      "q"  '("quit"           . bury-buffer)
      "G"  '("eww"            . eww)
      "g"  '("reload"         . eww-reload)
      "o"  '("links"          . ace-link-eww)
      "w"  '("copy-page-url"  . eww-copy-page-url)
      "d"  '("download"       . eww-download)
      "l"  '("back-url"       . eww-back-url)
      "r"  '("forward-url"    . eww-forward-url)
      "R"  '("reader-view"    . eww-readable)
      "n"  '("next-url"       . eww-next-url)
      "p"  '("previous-url"   . eww-previous-url)
      "H"  '("list-history"   . eww-list-histories)
      "b"  '("add-bookmark"   . eww-add-bookmark)
      "B"  '("list-bookmarks" . eww-list-bookmarks))))

;;; packages.el ends here
