;; -*- mode: emacs-lisp; fill-column: 75; -*-
;;
;;; packages.el --- ha-email layer packages file for Spacemacs.
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;;
;;; Commentary:
;;
;; Using MU to read my personal mail at a few of my email address inboxes.
;;
;;; Code:

(defconst ha-email-packages
  '(mu4e))

;; MU doesn't come with emacs, and needs to be installed as a package. While the
;; executable is available, the Emacs packages for MU is installed in different
;; places depending on the operating system. Doesn't take much to call `find' to
;; locate this and add it to `mu4e-installation-path':

(let* ((search-cmd "find /usr/local/Cellar/mu -name mu4e | sort --reverse | head -1")
       (search-results (shell-command-to-string search-cmd))
       (mac-path (string-trim-right search-results)))
  (unless (string-blank-p mac-path)
    (setq mu4e-installation-path mac-path)))

(defun ha-email/pre-init-mu4e ()
  "Overriding some MU4E Settings done before the package is loaded."
  (setq mu4e-compose-signature "(Why yes, this message was sent from Emacs)"
        mu4e-compose-signature-auto-include t
        mu4e-maildir "~/.mail"
        mu4e-get-mail-command "mbsync -a"
        mu4e-update-interval nil
        mu4e-compose-signature-auto-include nil
        mu4e-view-show-images t
        mu4e-use-fancy-chars t
        mu4e-view-show-addresses t
        message-kill-buffer-on-exit t))

(defun ha-email/post-init-mu4e ()
  "Overrides default MU4E bookmarks with my versions."
  (mu4e-bookmark-define "date:today..now AND NOT flag:trashed"
                        "Today's messages" ?t)
  (mu4e-bookmark-define "date:7d..now AND NOT flag:trashed"
                        "Last 7 days" ?w)
  (mu4e-bookmark-define "date:7d..now AND flag:unread AND NOT flag:trashed"
                        "Recent Unread messages" ?u)

  (use-package mu4e-context
    :load-path mu4e-installation-path
    :config
    (setq mu4e-contexts
          `( ,(make-mu4e-context
               :name "GooMail"
               :match-func (lambda (msg) (when msg
                                           (mu4e-message-contact-field-matches msg '(:to :cc) "howard.abrams@gmail.com")))
               :vars '((user-mail-address . "howard.abrams@gmail.com")
                       (smtpmail-starttls-credentials '(("smtp.googlemail.com" 587 nil nil)))))

             ,(make-mu4e-context
               :name "Personal"
               :match-func (lambda (msg) (when msg
                                           (mu4e-message-contact-field-matches msg '(:to :cc) "howard@howardabrams.com")))
               :vars '((user-mail-address . "howard@howardabrams.com")
                       (smtpmail-starttls-credentials '(("smtp.googlemail.com" 587 nil nil)))))))))

;;; packages.el ends here
