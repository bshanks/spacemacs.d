;;; LAYERS --- Pull in the Spacemacs MU4E layer
;;
;; Author: Howard X. Abrams <howard@howardabrams.com>
;; Copyright © 2020, Howard X. Abrams, all rights reserved.
;; Created:  7 May 2020
;;
;;; Code:


(configuration-layer/declare-layer '(mu4e :variables
                                          mail-user-agent 'mu4e-user-agent
                                          mu4e-use-maildirs-extension t
                                          org-mu4e-link-query-in-headers-mode nil
                                          mu4e-spacemacs-layout-name "@Mu4e"
                                          mu4e-spacemacs-layout-binding "M"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; layers.el ends here
