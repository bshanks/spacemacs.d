#!/bin/sh
# ----------------------------------------------------------------------
#  INSTALL_EMACS: Install Spacemacs
# ----------------------------------------------------------------------
set -e

# These are Alpine packages to install:
THE_PACK="dbus-x11 fontconfig mesa-gl emacs-x11"
apk add $THE_PACK

# Get Spacemacs
# git clone --branch develop https://github.com/syl20bnr/spacemacs /home/.emacs.d
# git clone https://gitlab.com/howardabrams/spacemacs.d.git /home/.spacemacs.d

echo =====================
echo   Installed Emacs
echo =====================
echo
