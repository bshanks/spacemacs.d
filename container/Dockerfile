## -*- docker-image-name: "howardabrams-spacemacs" -*-
#
# To build this:
#    docker build -t howardabrams-spacemacs .
# To run:
#    docker run -it howardabrams-spacemacs /bin/sh

ARG VERSION=latest
FROM ubuntu:$VERSION

MAINTAINER Howard Abrams <howard.abrams@gmail.com>

# Create "Inner" User Account
# ENV UNAME="howard" \
#   GNAME="users" \
#   HOME="/home/howard" \
#   SHELL="/bin/bash"

# RUN adduser --home $HOME --shell $SHELL --ingroup $GNAME --system --no-create-home $UNAME

WORKDIR /support
ADD . /support

# Fix "Couldn't register with accessibility bus" error message
ENV NO_AT_BRIDGE=1

ENV DEBIAN_FRONTEND noninteractive

# Notice the packages that we install into the image:
ENV PACKAGES="ack bash curl direnv fontconfig git gzip language-pack-en-base libgl1-mesa-glx ripgrep tar wget unzip"

# And packages that we install during build, and then remove afterwards:
ENV TMP_PACK="build-essential make libffi-dev"

RUN echo 'APT::Get::Assume-Yes "true";' >> /etc/apt/apt.conf
RUN apt-get update && apt-get install software-properties-common
RUN add-apt-repository -y ppa:x4121/ripgrep
RUN apt-get update && apt-get install $PACKAGES $TMP_PACK
RUN sh .docker_support/configure_linux.sh
RUN sh .docker_support/install_emacs.sh
RUN apt-get purge $TMP_PACK
RUN apt-get autoremove
RUN fc-cache -f
RUN rm -rf /tmp/* /var/lib/apt/lists/* /root/.cache/*

# WORKDIR $HOME
# ENTRYPOINT ["asEnvUser"] # ~/.dropbox-dist/dropboxd
# CMD ["bash", "-c", "emacs; /bin/bash"]

CMD ["bash"]
