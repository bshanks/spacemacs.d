#!/bin/sh
# ----------------------------------------------------------------------
#  SETUP_LAPTOP:
# ----------------------------------------------------------------------

mkdir -p ~/.fonts

if [ ! -f ~/.fonts/source-code-pro-roman.otf ]
then
    echo "## Getting Source Code Pro font..."
    curl --location --output ~/.fonts/source-code-pro-italic.otf https://github.com/adobe-fonts/source-code-pro/releases/download/variable-fonts/SourceCodeVariable-Italic.otf
    curl --location --output ~/.fonts/source-code-pro-roman.otf https://github.com/adobe-fonts/source-code-pro/releases/download/variable-fonts/SourceCodeVariable-Roman.otf
fi

if [ ! -f ~/.fonts/SourceSansPro-Regular.ttf ]
then
    echo "## Getting Source Sans Pro..."
    curl --location --output ~/Downloads/source-sans-pro.zip https://github.com/adobe-fonts/source-sans-pro/archive/3.006R.zip
    unzip ~/Downloads/source-sans-pro.zip -d ~/.fonts/
    cp ~/.fonts/source-sans-pro-*/TTF/*.ttf ~/.fonts/
    rm -rf ~/.fonts/source-sans-pro-*
fi

if [ ! -f ~/.fonts/SourceSerifPro-Regular.ttf ]
then
    echo "## Getting Source Serif Pro..."
    curl --location --output ~/Downloads/source-serif-pro.zip https://github.com/adobe-fonts/source-serif-pro/archive/3.001R.zip
    unzip ~/Downloads/source-serif-pro.zip -d ~/.fonts/
    cp ~/.fonts/source-serif-pro-*/TTF/*.ttf ~/.fonts/
    rm -rf ~/.fonts/source-serif-pro-*
fi

if [ ! -f ~/.fonts/Hasklig-Medium.otf ]
then
    echo "## Getting Hasklig Font..."
    curl --location --output ~/Downloads/hasklig.zip https://github.com/i-tu/Hasklig/releases/download/1.1/Hasklig-1.1.zip
    unzip ~/Downloads/hasklig.zip -d ~/.fonts/
fi

if [ ! -f ~/.fonts/iosevka-medium.ttf ]
then
    echo "## Getting Iosevka font..."
    curl --location --output ~/Downloads/iosevka.zip https://github.com/be5invis/Iosevka/releases/download/v2.3.1/01-iosevka-2.3.1.zip
    unzip ~/Downloads/iosevka.zip -d ~/.fonts/

    echo "## Getting Iosevka Slab font..."
    curl -location --output ~/Downloads/iosevka-slab.zip https://github.com/be5invis/Iosevka/releases/download/v2.3.1/05-iosevka-slab-2.3.1.zip
    unzip ~/Downloads/iosevka-slab.zip -d ~/.fonts/

    cp ~/.fonts/ttf/*.ttf ~/.fonts/
    rm -rf ~/.fonts/ttf ~/.fonts/ttf-unhinted ~/.fonts/woff ~/.fonts/woff2 ~/.fonts/webfont.css
fi
